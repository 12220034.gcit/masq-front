export const API_URL = process.env.NEXT_PUBLIC_API_URL || 'https://massapi.onrender.com'
export const WEBSOCKET_URL = process.env.NEXT_PUBLIC_WEBSOCKET_URL || 'wss://massapi.onrender.com'