import { useState, createContext, useEffect } from 'react';
import { useRouter } from 'next/router';

export type UserInfo = {
  username: string;
  id: string;
};

export const AuthContext = createContext<{
  authenticated: boolean;
  setAuthenticated: (auth: boolean) => void;
  user: UserInfo;
  setUser: (user: UserInfo) => void;
  logout: () => void; // Add logout function to the context
}>({
  authenticated: false,
  setAuthenticated: () => {},
  user: { username: '', id: '' },
  setUser: () => {},
  logout: () => {}, // Initialize logout function
});

const AuthContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [authenticated, setAuthenticated] = useState(false);
  const [user, setUser] = useState<UserInfo>({ username: '', id: '' });
  const router = useRouter();

  useEffect(() => {
    const userInfo = localStorage.getItem('user_info');

    if (!userInfo) {
      if (window.location.pathname !== '/signup') {
        router.push('/login');
        return;
      }
    } else {
      const user: UserInfo = JSON.parse(userInfo);
      if (user) {
        setUser({
          username: user.username,
          id: user.id,
        });
      }
      setAuthenticated(true);
    }
  }, []);

  const logout = () => {
    // Clear the token and user info
    localStorage.removeItem('user_info');
    setUser({ username: '', id: '' });
    setAuthenticated(false);
  };

  return (
    <AuthContext.Provider
      value={{
        authenticated: authenticated,
        setAuthenticated: setAuthenticated,
        user: user,
        setUser: setUser,
        logout: logout, // Provide the logout function
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
