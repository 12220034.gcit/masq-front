import { useState, useContext, useEffect } from 'react';
import { API_URL } from '../../constants';
import { useRouter } from 'next/router';
import { AuthContext, UserInfo } from '../../modules/auth_provider';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Index = (): JSX.Element => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isError, setIsError] = useState(false); // State for error border
  const { authenticated } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    if (authenticated && router.pathname !== '/signup') {
      router.push('/');
      return;
    }
  }, [authenticated, router]);

  const submitHandler = async (e: React.SyntheticEvent) => {
    e.preventDefault();

    try {
      const res = await fetch(`${API_URL}/login`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password }),
      });

      if (res.ok) {
        const data = await res.json();
        const user: UserInfo = {
          username: data.username,
          id: data.id,
        };

        localStorage.setItem('user_info', JSON.stringify(user));
        return router.push('/');
      } else {
        throw new Error('Wrong Email/Password');
      }
    } catch (err) {
      console.log(err);
      setIsError(true); // Set isError state to true
      toast.error((err as Error).message);
    }
  };

  const redirectToSignup = () => {
    router.push('/signup');
  };

  return (
    <div className='flex items-center justify-center min-w-full min-h-screen'>
      <form className='flex flex-col md:w-1/5'>
        <div className='text-3xl font-bold text-center'>
          <span className='text-blue'>welcome!</span>
        </div>
        <input
          placeholder='email'
          className={`p-3 mt-8 rounded-md border-2 ${
            isError ? 'border-red' : 'border-grey'
          } focus:outline-none focus:border-blue`}
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
            setIsError(false); // Reset isError state on input change
          }}
        />
        <input
          type='password'
          placeholder='password'
          className={`p-3 mt-4 rounded-md border-2 ${
            isError ? 'border-red' : 'border-grey'
          } focus:outline-none focus:border-blue`}
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
            setIsError(false); // Reset isError state on input change
          }}
        />
        <button
          className='p-3 mt-6 rounded-md bg-blue font-bold text-white'
          type='submit'
          onClick={submitHandler}
        >
          login
        </button>

        <button
          className='mt-4 text-blue'
          type='button'
          onClick={redirectToSignup}
        >
          Don't have an account? Create one now
        </button>
      </form>
      <ToastContainer />
    </div>
  );
};

export default Index;
