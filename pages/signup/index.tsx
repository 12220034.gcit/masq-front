import { useState, useContext, useEffect, SyntheticEvent } from 'react';
import { API_URL } from '../../constants';
import { useRouter } from 'next/router';
import { AuthContext } from '../../modules/auth_provider';

const Index = (): JSX.Element => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [username, setUsername] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isUsernameValid, setIsUsernameValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const { authenticated } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    if (authenticated) {
      router.push('/');
    }
  }, [authenticated]);

  const validateEmail = (email: string): boolean => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const validateUsername = (username: string): boolean => {
    const usernameRegex = /^[a-zA-Z0-9]+$/;
    return usernameRegex.test(username);
  };

  const validatePassword = (password: string): boolean => {
    const passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
    return passwordRegex.test(password);
  };

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    setEmail(value);
    setIsEmailValid(validateEmail(value));
  };

  const handleUsernameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    setUsername(value);
    setIsUsernameValid(validateUsername(value));
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    setPassword(value);
    setIsPasswordValid(validatePassword(value));
  };

  const submitHandler = async (e: SyntheticEvent): Promise<void> => {
    e.preventDefault();

    if (!validateEmail(email) || !validateUsername(username) || !validatePassword(password) || password !== confirmPassword) {
      return;
    }

    try {
      const res = await fetch(`${API_URL}/signup`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password, username }),
      });

      if (res.ok) {
        router.push('/login');
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className='flex items-center justify-center min-w-full min-h-screen'>
      <form className='flex flex-col md:w-1/5'>
        <div className='text-3xl font-bold text-center'>
          <span className='text-blue'>Sign Up</span>
        </div>
        <div className='mt-4'>
          {!isUsernameValid && (
            <div className='text-red'>Username can only consist of alphabets and numbers</div>
          )}
          <input
            placeholder='username'
            className={`p-3 mt-2 rounded-md border-2 w-full ${isUsernameValid ? 'border-grey' : 'border-red'} focus:outline-none ${isUsernameValid ? 'focus:border-blue' : ''}`}
            value={username}
            onChange={handleUsernameChange}
          />
        </div>
        <div className='mt-4'>
          {!isEmailValid && (
            <div className='text-red'>Please enter a valid email address</div>
          )}
          <input
            placeholder='email'
            className={`p-3 mt-2 rounded-md border-2 w-full ${isEmailValid ? 'border-grey' : 'border-red'} focus:outline-none ${isEmailValid ? 'focus:border-blue' : ''}`}
            value={email}
            onChange={handleEmailChange}
          />
        </div>
        <div className='mt-4'>
          {!isPasswordValid && (
            <div className='text-red'>Password must contain at least 8 characters, including a number and a special character</div>
          )}
          <input
            type='password'
            placeholder='password'
            className={`p-3 mt-2 rounded-md border-2 w-full ${isPasswordValid ? 'border-grey' : 'border-red'} focus:outline-none ${isPasswordValid ? 'focus:border-blue' : ''}`}
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <input
          type='password'
          placeholder='confirm password'
          className={`p-3 mt-4 rounded-md border-2 w-full ${password === confirmPassword ? 'border-grey' : 'border-red'} focus:outline-none ${password === confirmPassword ? 'focus:border-blue' : ''}`}
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        <button
          className='p-3 mt-6 rounded-md bg-blue font-bold text-white'
          type='submit'
          onClick={submitHandler}
        >
          Sign Up
        </button>
        <div className='mt-4 text-center'>
          Already have an account?{' '}
          <a className='text-blue' href='/login'>
            Log in
          </a>
        </div>
      </form>
    </div>
  );
};

export default Index;
